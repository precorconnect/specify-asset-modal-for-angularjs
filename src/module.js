import angular from 'angular';
import SpecifyAssetModal from './specifyAssetModal';
import 'add-asset-modal';
import errorMessagesTemplate from './error-messages.html!text';
import 'angular-bootstrap';
import 'angular-messages';
import 'bootstrap/css/bootstrap.css!css'
import 'bootstrap'

angular
    .module(
        'specifyAssetModal.module',
        [
            'ui.bootstrap',
            'ngMessages',
            'addAssetModal.module'
        ]
    )
    .service(
        'specifyAssetModal',
        [
            '$modal',
            SpecifyAssetModal
        ]
    )
    .run(
        [
            '$templateCache',
            $templateCache => {
                $templateCache.put(
                    'specify-asset-modal/error-messages.html',
                    errorMessagesTemplate
                );
            }
        ]
    );