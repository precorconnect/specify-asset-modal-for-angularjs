import specifyAssetTemplate from './specify-asset.html!text';
import SpecifyAssetController from './specifyAssetController';

export default class RouteConfig {

    constructor($routeProvider) {

        $routeProvider
            .when
            (
                '/',
                {
                    template: specifyAssetTemplate,
                    controller: SpecifyAssetController,
                    controllerAs: 'controller'
                }
            );

    }

}

RouteConfig.$inject = [
    '$routeProvider'
];