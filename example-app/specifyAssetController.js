import {SpecifyAssetModalConfig} from '../src/index'

export default class SpecifyAssetController {

    _specifyAssetModal;

    _specifiedAssets = [];

    constructor(specifyAssetModal) {

        if (!specifyAssetModal) {
            throw new TypeError('specifyAssetModal required');
        }
        this._specifyAssetModal = specifyAssetModal;

    }

    get specifiedAssets() {
        return this._specifiedAssets;
    }

    showSpecifyAssetModal() {

        this._specifyAssetModal
            .show(
                new SpecifyAssetModalConfig(
                    this._specifiedAssets.map(element => element.id),
                    assets => {
                        this._specifiedAssets = this._specifiedAssets.concat(assets);
                    }
                )
            );

    }

}

SpecifyAssetController.$inject = [
    'specifyAssetModal'
];
